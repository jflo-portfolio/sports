#!/usr/bin/env python3
from io import StringIO

import pandas as pd
import requests
from lxml import html

# query = urllib.parse.quote("lebron james")
# search_str = "https://www.espn.com/search/_/type/players/q/{0}".format(query)
# sess = requests.Session()
# r = sess.get(search_str)
# print(r.text)
# f = StringIO(r.text)

# tree = html.parse(f)
# e = tree.xpath("//a[@data-track-linkname='search:resultselected'][1]/@href")
# print(e)

# sess = requests.Session()
# res = sess.get("https://www.espn.com/nba/player/_/id/1966/lebron-james")
# f = StringIO(res.text)
# page = html.parse(f)
# bio_elem = page.xpath("//ul[contains(@class,'PlayerHeader__Bio_List')][1]")[0]
# elem = bio_elem.xpath("//div[contains(text(), 'HT/WT')]/following-sibling::div")[0]
# print(elem.text_content().strip())


def text(elt):
    return elt.text_content().replace(u"\xa0", u" ")


sess = requests.Session()
res = sess.get(
    "https://www.espn.com/nfl/player/stats/_/id/3139477/patrick-mahomes"
)
f = StringIO(res.text)
page = html.parse(f)
titles = page.xpath("//div[@class = 'Table__Title']")


def table2frame(table_elem):
    header = [text(th) for th in table_elem.xpath("descendant::th")]

    data = [
        [text(td) for td in tr.xpath("td")]
        for tr in table_elem.xpath("descendant::tr")
    ]
    data = [row for row in data if len(row) == len(header)]
    data = pd.DataFrame(data, columns=header)
    return data


stats_dict = dict()
for t in titles:
    table1 = t.xpath("following::div/table")[0]
    table2 = t.xpath("following::div/table")[1]
    df1 = table2frame(table1)
    df2 = table2frame(table2)
    frames = [df1, df2]
    result = pd.concat(frames, axis=1, join="inner")
    stats_dict[t.text_content()] = result


print(stats_dict["Passing"])


# table = t.xpath("following::div/table")[1]
# header = [text(th) for th in table.xpath('descendant::th')]

# data = [[text(td) for td in tr.xpath('td')]
#         for tr in table.xpath('descendant::tr')]
# data = [row for row in data if len(row)==len(header)]
# data = pd.DataFrame(data, columns=header)
# print(data)


# table = t.xpath("following::div/table")[1]
# print(t.text_content())
# for header in table.xpath("descendant::th[@class = 'Table__TH']"):
#     print(header.text_content())
# for td in table.xpath("descendant::td[@class = 'Table__TD']"):
#     print(td.text_content())

# table2 = t.xpath("following::div/table")[1]
# for header in table2.xpath("descendant::th[@class = 'Table__TH']"):
#     print(header.text_content())
# for td in table.xpath("descendant::td[@class = 'Table__TD']"):
#     print(td.text_content())

# print(elem.text_content().strip())
# //*[@id="fittPageContainer"]/div[2]/div[5]/div/div/div[1]/section/div/div[3]/div[2]
# https://stackoverflow.com/questions/28305578/python-get-html-table-data-by-xpath

# https://www.espn.com/nfl/player/_/id/5536/ben-roethlisberger
# https://www.espn.com/nfl/player/stats/_/id/5536/ben-roethlisberger
# https://www.espn.com/nfl/player/stats/_/id/5536/type/nfl/seasontype/3
