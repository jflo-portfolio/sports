import pandas as pd
import pytest

from sports import team


@pytest.fixture(scope="module", params=["chiefs", "los angeles lakers"])
def team_obj(request):
    t = team(request.param)
    return t


def test_name(team_obj):
    print(team_obj.name)
    assert team_obj.name in ["Kansas City Chiefs", "Los Angeles Lakers"]


def test_roster(team_obj):
    print(team_obj.roster)
    assert isinstance(team_obj.roster, pd.core.frame.DataFrame)


def test_schedule(team_obj):
    print(team_obj.schedule)
    assert isinstance(team_obj.schedule, pd.core.frame.DataFrame)
