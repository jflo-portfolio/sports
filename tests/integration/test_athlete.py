import pytest

from sports import athlete


@pytest.fixture(scope="module", params=["patrick mahomes"])
def athlete_obj(request):
    a = athlete(request.param)
    return a


def test_url(athlete_obj):
    print(athlete_obj.url)
    assert (
        athlete_obj.url
        == "http://www.espn.com/nfl/player/_/id/3139477/patrick-mahomes"
    )


def test_name(athlete_obj):
    print(athlete_obj.name)
    assert athlete_obj.name == "Patrick Mahomes"


def test_ht_wt(athlete_obj):
    print(athlete_obj.ht_wt)
    assert athlete_obj.ht_wt == "6' 3\", 227 lbs"


def test_dob(athlete_obj):
    print(athlete_obj.dob)
    assert athlete_obj.dob == "9/17/1995 (26)"


def test_status(athlete_obj):
    print(athlete_obj.status)
    assert athlete_obj.status == "Active"


def test_experience(athlete_obj):
    print(athlete_obj.experience)
    assert athlete_obj.experience is None


def test_draft_info(athlete_obj):
    print(athlete_obj.draft_info)
    assert athlete_obj.draft_info == "2017: Rd 1, Pk 10 (KC)"


def test_college(athlete_obj):
    print(athlete_obj.college)
    assert athlete_obj.college == "Texas Tech"


@pytest.mark.parametrize("season_type", ["regular_season", "post_season"])
def test_stats(athlete_obj, season_type):
    print(athlete_obj.stats[season_type])


def show_athlete():
    a = athlete("patrick mahomes")
    print(a.url)
    print(a.name)
    print(a.ht_wt)
    print(a.dob)
    print(a.status)
    print(a.experience)
    print(a.draft_info)
    print(a.college)
    print(a._stats_pages())
    print(a.stats["regular_season"])
