import pandas as pd
import pytest

import sports

from ..integration import test_team


class MockTeam:
    @property
    def name(self):
        return "Kansas City Chiefs"

    @property
    def roster(self):
        return pd.DataFrame()

    @property
    def schedule(self):
        return pd.DataFrame()


@pytest.fixture
def team_obj(monkeypatch):
    def mock_team(*args, **kwargs):
        return MockTeam()

    monkeypatch.setattr(sports, "team", mock_team)

    t = sports.team("chiefs")
    return t


def test_name(team_obj):
    test_team.test_name(team_obj)


def test_roster(team_obj):
    test_team.test_roster(team_obj)


def test_schedule(team_obj):
    test_team.test_schedule(team_obj)
