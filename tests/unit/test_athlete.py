import pytest

import sports

from ..integration import test_athlete


class MockAthlete:
    @property
    def name(self):
        return "Patrick Mahomes"

    @property
    def url(self):
        return "http://www.espn.com/nfl/player/_/id/3139477/patrick-mahomes"

    @property
    def ht_wt(self):
        return "6' 3\", 227 lbs"

    @property
    def dob(self):
        return "9/17/1995 (26)"

    @property
    def status(self):
        return "Active"

    @property
    def experience(self):
        return None

    @property
    def draft_info(self):
        return "2017: Rd 1, Pk 10 (KC)"

    @property
    def college(self):
        return "Texas Tech"

    @property
    def stats(self):
        pass


@pytest.fixture
def athlete_obj(monkeypatch):
    def mock_athlete(*args, **kwargs):
        return MockAthlete()

    monkeypatch.setattr(sports, "athlete", mock_athlete)

    a = sports.athlete("patrick mahomes")
    return a


def test_name(athlete_obj):
    test_athlete.test_name(athlete_obj)


def test_url(athlete_obj):
    test_athlete.test_url(athlete_obj)


def test_ht_wt(athlete_obj):
    test_athlete.test_ht_wt(athlete_obj)


def test_dob(athlete_obj):
    test_athlete.test_dob(athlete_obj)


def test_status(athlete_obj):
    test_athlete.test_status(athlete_obj)


def test_experience(athlete_obj):
    test_athlete.test_experience(athlete_obj)


def test_draft_info(athlete_obj):
    test_athlete.test_draft_info(athlete_obj)


def test_college(athlete_obj):
    test_athlete.test_college(athlete_obj)
