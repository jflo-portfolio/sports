from sports.data.athlete import Athlete
from sports.data.team import Team


def athlete(athlete_name):
    """Return an Athlete object

    :param athlete_name: the name of an Athlete
    :type athlete_name: str
    :return: a Athlete object
    :rtype: class:`sports.data.athlete.Athlete`
    """
    return Athlete(athlete_name)


def team(team_name):
    """Return a Team object

    :param team_name: the name of a Team
    :type teme_name: str
    :return: a Team object
    :rtype: class:`sports.data.team.Team`
    """
    return Team(team_name)
