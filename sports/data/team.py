import re

import pandas as pd

from sports.data.sports_data import SportsData


class Team(SportsData):
    """This is a class representing a team

    :param team: team to search for
    :type team: str
    """

    def __init__(self, team):
        """Constructor method"""
        super().__init__()
        self._url = super().search(team, type="team")
        self._roster_url = re.sub(
            r"(.*)(_/name/[a-z]{2,3}/).*", "\\1roster/\\2", self._url
        )
        self._schedule_url = re.sub(
            r"(.*)(_/name/[a-z]{2,3}/).*", "\\1schedule/\\2", self._url
        )
        # self._page = self._parse_page(self._url)

    @property
    def name(self):
        """team name

        :return: team name
        :rtype: str
        """
        match = re.search(".*/(.*)$", self._url)
        slug_name = match.group(1)
        team_name = " ".join(slug_name.split("-")).title()
        return team_name

    @property
    def roster(self):
        """team roster

        :return: dataframe of team's roster
        :rtype: `pd.DataFrame`
        """
        roster_page = self._parse_page(self._roster_url)
        tables = roster_page.xpath('//table[@class="Table"]')
        table_l = list()
        for t_elem in tables:
            table_l.extend(
                [remove_ticket_text(td) for td in tr.xpath("td")]
                for tr in t_elem.xpath("descendant::tr")
            )

        return pd.DataFrame(table_l)

    @property
    def schedule(self):
        """team schedule

        :return: a team's schedule
        :rtype: `pd.DataFrame`
        """
        schedule_page = self._parse_page(self._schedule_url)
        t_elem = schedule_page.xpath(
            "//div[@class='Table__Scroller'][1]/table"
        )[0]
        table_l = list()
        table_l = [
            [remove_ticket_text(td) for td in tr.xpath("td")]
            for tr in t_elem.xpath("descendant::tr")
        ]
        return pd.DataFrame(table_l)

    def __repr__(self):
        return "<sports.team - {}>".format(self.name)

    def __str__(self):
        return self.name


def table2frame(table_elem):
    """Utility method tat converts a table to a pandas dataframe

    :param table_elem: html table element
    :type table_elem: `lxml.html.HtmlElement`
    :return: data frame
    :rtype: `pd.DataFrame`
    """
    header = [
        remove_ticket_text(th) for th in table_elem.xpath("descendant::th")
    ]
    print(type(table_elem))
    data = [
        [remove_ticket_text(td) for td in tr.xpath("td")]
        for tr in table_elem.xpath("descendant::tr")
    ]
    data = [row for row in data if len(row) == len(header)]
    data = pd.DataFrame(data, columns=header)
    return data


def remove_ticket_text(elt):
    """utility method to remove "Ticket" links from the schedule.

    :param elt: html element
    :type elt: 'lxml.html.HtmlElement'
    :return: cleaned up string
    :rtype: str
    """
    return re.sub(r"[Tt]ickets.*", "", elt.text_content())
