import re

import pandas as pd

from sports.data.sports_data import SportsData


class Athlete(SportsData):
    """This is a class representing an athlete

    :param athlete: athlete to search for
    :type athlete: str
    """

    def __init__(self, athlete):
        """Constuctor method"""
        super().__init__()
        self._url = super().search(athlete, type="athlete")
        self._page = self._parse_page(self._url)

    def _bio_elem(self):
        bio_elem = self._page.xpath(
            "//ul[contains(@class,'PlayerHeader__Bio_List')][1]"
        )[0]
        return bio_elem

    def _bio_info(self, field):
        try:
            bio_xpath = "//div[contains(text(), '{}')]/following-sibling::div"
            elem = self._bio_elem().xpath(bio_xpath.format(field))[0]
            return elem.text_content().strip()
        except IndexError:
            return None

    def _stats_tables(self, url):
        page = self._parse_page(url)
        titles = page.xpath("//div[@class = 'Table__Title']")
        stats_dict = dict()
        for title in titles:
            table1 = title.xpath("following::div/table")[0]
            table2 = title.xpath("following::div/table")[1]
            df1 = table2frame(table1)
            df2 = table2frame(table2)
            frames = [df1, df2]
            result = pd.concat(frames, axis=1, join="inner")
            stats_dict[title.text_content()] = result
        return stats_dict

    def _stats_pages(self):
        stats1 = self.url.replace("/player/", "/player/stats/")
        stats2 = re.sub(
            r"https?://www.espn.com/([a-z]+)/player/_/id/([0-9]+)/.*",
            "https://www.espn.com/\\1/player/stats/_/id/\\2/type/\\1/seasontype/3",
            self.url,
        )
        return [stats1, stats2]

    @property
    def url(self):
        """Return athlete page url

        :return: page url
        :rtype: str
        """
        return self._url

    @property
    def name(self):
        """Athlete's name

        :return: athlete name
        :rtype: str
        """
        name_elem = self._page.xpath(
            "//h1[contains(@class,'PlayerHeader__Name')][1]"
        )[0]
        first = name_elem.xpath("child::span")[0].text_content()
        last = name_elem.xpath("child::span")[1].text_content()
        return "{0} {1}".format(first, last)

    @property
    def ht_wt(self):
        """Height and Weight

        :return: height and weight
        :rtype: str
        """
        return self._bio_info("HT/WT")

    @property
    def dob(self):
        """Date of birth

        :return: dob
        :rtype: str
        """
        return self._bio_info("DOB")

    @property
    def college(self):
        """College

        :return: college
        :rtype: str
        """
        return self._bio_info("College")

    @property
    def draft_info(self):
        """Draft info

        :return: draft info
        :rtype: str
        """
        return self._bio_info("Draft Info")

    @property
    def status(self):
        """Status

        whether or not
        athlete is active

        :return: status
        :rtype: str
        """
        return self._bio_info("Status")

    @property
    def experience(self):
        """Experience

        :return: experience
        :rtype: str
        """
        return self._bio_info("Experience")

    @property
    def stats(self):
        """Athlete stats

        :return: dictionary of athlete stats:
        {'regular_season': `pd.DataFrame`, 'post_season': `pd.DataFrame`}
        :rtype: dict
        """
        stats_type = dict()
        stats_type["regular_season"] = self._stats_tables(
            self._stats_pages()[0]
        )
        stats_type["post_season"] = self._stats_tables(self._stats_pages()[1])

        return stats_type

    def __repr__(self):
        return "<sports.athlete - {}>".format(self.name)

    def __str__(self):
        return self.name


def table2frame(table_elem):
    """Utility method that converts a table to a dataframe

    :param table_elem: html table element
    :type table_elem: `lxml.html.HtmlElement`
    :return: data frame
    :rtype: `pd.DataFrame`
    """
    header = [
        remove_ticket_text(th) for th in table_elem.xpath("descendant::th")
    ]

    data = [
        [remove_ticket_text(td) for td in tr.xpath("td")]
        for tr in table_elem.xpath("descendant::tr")
    ]
    data = [row for row in data if len(row) == len(header)]
    data = pd.DataFrame(data, columns=header)
    return data


def remove_ticket_text(elt):
    """utility method to remove "Ticket" links from the schedule.

    :param elt: html element
    :type elt: 'lxml.html.HtmlElement'
    :return: cleaned up string
    :rtype: str
    """
    return re.sub(r"[Tt]ickets.*", "", elt.text_content())
