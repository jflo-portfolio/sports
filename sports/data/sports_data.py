import urllib.parse
from io import StringIO

import requests
from lxml import html
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class SportsData:
    """Base SportsData Class

    sets up a requests sessions
    contains convenience methods to parse a page and search espn
    """

    def __init__(self):
        """Constructor method"""
        self._sess = requests.Session()

    def _parse_page(self, url):
        """Method to parse an html page

        :param url: a url
        :type url: str
        :return: returns an _ElementTree object so we can
            find/get contents of the page.
        :rtype: class `lxml.etree._ElementTree`
        """
        res = self._sess.get(url)
        fobj = StringIO(res.text)
        return html.parse(fobj)

    def search(self, query, type=None):
        """Search espn

        :param query: query to search for athlete or team
        :type query: str
        :param qtype: athlete or team
        :type qtype: str or None, optional
        :return: returns the url of the first search result
        :rtype: str
        """
        options = Options()
        options.add_argument("--headless")
        options.add_argument("window-size=412x869")
        driver = webdriver.Chrome(options=options)

        search_str = urllib.parse.quote(query)
        if type == "athlete":
            driver.get(
                "https://www.espn.com/search/_/type/players/q/{}".format(
                    search_str
                )
            )
        elif type == "team":
            driver.get(
                "https://www.espn.com/search/_/type/teams/q/{}".format(
                    search_str
                )
            )

        if "/search/_/type/" not in driver.current_url:
            # no results
            return None

        a_elem = driver.find_element_by_xpath(
            "//a[@data-track-linkname='search:resultselected'][1]"
        )
        current_url = a_elem.get_attribute("href")
        driver.quit()
        return current_url
