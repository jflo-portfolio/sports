# sports

sports is a Python library that scrapes espn.com for information on
players and teams.


## Installation

### Python version

Python (>= 3.6)

### Install sports_scrape

    pip install git+https://bitbucket.org/jflo-portfolio/sports.git

### Install ChromeDriver
This library uses Selenium and the Chrome browser in headless mode. So you will
need to download and install ChromeDriver:
https://sites.google.com/a/chromium.org/chromedriver/home

## Usage

import module

        >>> import sports

create a player object

        >>> player = sports.athlete("LeBron James")

information available for a player:

        >>> player.name
        >>> player.url
        >>> player.ht_wt
        >>> player.dob
        >>> player.status
        >>> player.experience
        >>> player.draft_info
        >>> player.college
        >>> player.stats

create a team object

        >>> team = sports.team("Lakers")

information available for a team:

        >>> team.name
        >>> team.roster
        >>> team.schedule