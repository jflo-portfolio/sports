#!/usr/bin/env python3
from sports import athlete
from sports import team


def show_athlete():
    a = athlete("patrick mahomes")
    print(a.url)
    print(a.name)
    print(a.ht_wt)
    print(a.dob)
    print(a.status)
    print(a.experience)
    print(a.draft_info)
    print(a.college)
    print(a._stats_pages())
    print(a.stats["regular_season"])


def show_team():
    t = team("chiefs")
    print(t.roster)
    print(t.schedule)


show_athlete()
show_team()
